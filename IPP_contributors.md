Marche à suivre pour modifier les barèmes pour les contributeurs issus de l'IPP
===============================================================================

1. Rassembler les informations concernant les barèmes à modifier.

2. Ouvrir une console [shell] 
    (https://git.framasoft.org/ipp/ipp-survival-gitbook/blob/master/shell.md) 
    et se placer dans le répertoire contenant les barèmes (ne pas hésiter à 
    utiliser la touche <kbd>Tab</kbd> pour faire apparaître les noms des sous-dossiers.)

    ````shell
    cd "Z:\3-Legislation\Barèmes IPP"
    ````
   NB: On travaille sur la branche appelée master en local (sur notre ordinateur).
   
    ````shell
    cd "Z:\3-Legislation\Barèmes IPP" <master>
    ````
3. Récupérer les informations sur l'ensemble des dépôts distants, i.e. récupérer les modifications faites et validées par les autres utilisateurs.

    ````shell
    git fetch --all
    ````
4. Vérifier où se situe notre branche en local (i.e sur notre ordi) par rapport à la branche du dépôt IPP sur Gitlab (appelée IPP/master) en éxecutant:

    ````shell
    gitk --all
    ````
5. Afin de se synchroniser avec la branche du dépôt IPP sur Gitlab, on importe les modifications propagées par les autres utilisateurs en éxecutant:    
    
    ````shell
    git rebase ipp/master
    ````    
6. Vérifier l'état du dépôt en exécutant, ce qui nous permet de voir si des fichiers suivis ("tracked files") ont été modifiés sans avoir été commités. 
   
    ````shell
    git status 
    ````
7. Si les fichiers à modifier sont inchangés allez au point directement au point 6.
   Si un fichier à modifier est altéré depuis le dernier "commit", commiter les modifications en éxecutant:
    
    ````shell
    git commit
    ````

8. Modifier son fichier excel, enregistrer les modifications, puis fermer le document.

9. Puis en utilisant le shell, enregistrer ses modifications dans le système de version local
   en prenant soin d'effectuer les deux étapes suivantes:
  * n'ajouter que le/les fichier(s) modifiés à l'index en utilisant: 

    ````shell
    git add monfichier.xlsx
    ````
    ou en utilisant l'interface accessible en exécutant `git-gui`

    ````shell
    git gui
    ````
  * puis commiter les modifications en ajoutant un message de "commit" [compréhensible par tous] 
    (http://chris.beams.io/posts/git-commit/) 
    indiquant les modifications effectuées en exécutant
    
    ````shell
    git commit
    ````
    S'il y a lieu, ne pas oublier 
    d'indiquer le ticket ("issue") qui est ainsi fermé en ajoutant dans le message
    de commit le texte ci-dessous:
    
    ````txt
    Fais plein de modifications très utiles
    Closes #NumeroDuTicket`
    ````

8. Enfin, propager les modifications au [dépot gitlab hébergé par framasoft] 
   (https://git.framasoft.org/french-tax-and-benefit-tables/ipp-tax-and-benefit-tables-xlsx/)
   en exécutant
    
   ````shell
   git push ipp master
   ````
   ou en utilisant l'interface accessible en exécutant `git-gui`
   ````shell
   git gui
   ````
9. Pour bien vérifier que ma version en local est parfaitement synchronisée avec la version à jour sur Gitlab, faire une ultime vérification en éxécutant à nouveau:
    
    ````shell
    gitk --all
    ````