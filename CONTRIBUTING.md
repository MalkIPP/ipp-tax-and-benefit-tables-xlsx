Contribuer aux barèmes IPP
==========================

Ces barèmes sont régulièrement actualisés avec les changements de législation et mis en ligne chaque année en avril.
Les équipes de l’IPP complètent progressivement ces documents à la fois dans leur dimension historique et leur prise 
en compte de l’ensemble de la législation des politiques publiques. 
Nous invitons les utilisateurs à nous faire part d’erreurs ou de compléments à apporter à ces documents: baremes@ipp.eu

Ces barèmes ont bénéficié d’un financement du Labex OSE

Pour les membres de l'IPP, le guide du contributeur se trouve [ici] (https://git.framasoft.org/french-tax-and-benefit-tables/ipp-tax-and-benefit-tables-xlsx/blob/master/IPP_contributors.md)